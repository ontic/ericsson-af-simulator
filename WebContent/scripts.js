////////////////////////////////////////////////////////////////////////////////
////                                                                        //// 
//// COPYRIGHT (c) Ericsson AB 2016                                         //// 
////                                                                        //// 
//// The copyright to the computer Program(s) herein is the                 //// 
//// property of Ericsson AB, Sweden. The program(s) may be                 //// 
//// used and or copied only with the written permission of                 //// 
//// Ericsson AB, or in accordance with the terms and conditions            //// 
//// stipulated in the agreement contract under which the                   //// 
//// program(s) have been supplied.                                         //// 
////                                                                        //// 
////////////////////////////////////////////////////////////////////////////////

// Constants
var LOGO = "ontic";
var VERSION = "0.7";
var FOOTER = "<p>Prediction Report manager (release <label>" + VERSION + "</label>)<br/> \
          A fine work by <label>Technology and Innovation, Ericsson Spain</label><br/> \
          &copy; LM Ericsson 2016</p>";
var MAIN_PAGE = "GeneratePrediction.jsp";

// Global variables for controlling input
var confidence = false;
var start_date = false;
var end_date   = false;
var start_time = true;
var end_time   = true;
var kpis       = false;
var groups     = false;

//// Functions
// To do at upload
$(function() {
	//console.log($(location).attr('href'));
	if ($(location).attr('href').includes(MAIN_PAGE)) {
		$("#starttimecalendar").datepicker({ dateFormat: 'dd-mm-yy' });
		$("#endtimecalendar").datepicker({ dateFormat: 'dd-mm-yy' });
		$('[data-toggle="tooltip"]').tooltip('show');
	}
	$('#logo').attr('id','logo_'+LOGO);
	$('.footer').html(FOOTER);
});

// Functions to fill the initial fields of the form
function insert_ReportId_Into_Form() {
   $.get("./ReportIdGenerator", function(data) {
      console.log ("Inserting report ID");
      Report_id_ran = data;
      var field = document.forms.predictionForm.elements;
      field.reportId.value= Report_id_ran;
   });
}

function insert_LocationList_Into_Form() {
   $.get("./Location_List_From_DB", function(data){
      console.log ("Inserting location");
      Location_list = data;
      location_object_list = JSON.parse(data);
      var field = document.forms.predictionForm.elements.locationid;
      var list="";
      for (var i=0;i<location_object_list.length;i++) {
         var location = new Option(location_object_list[i].cellId + " : " + location_object_list[i].city,
                                   location_object_list[i].city + " : " + location_object_list[i].cellId,
                                   false,
                                   false);
         field.options[field.options.length] = location;
      }
   });
}

function insert_TimeStamp_Into_Form() {
   console.log ("Inserting timestamp");
   var field = document.forms.predictionForm.elements;
   var date = new Date();
   field.timestamp.value=date.toString().split('(')[0].slice(0, -1);
   setTimeout(insert_TimeStamp_Into_Form,1000);
}

function insert_services_and_kpis_Into_Form() {
   $.get("./Service_And_KPIs_Servlet", function(data){
      console.log ("Inserting Services and KPI's");
      kpi_list=data;
      kpi_object_list = JSON.parse(data);
      var row = $("#tablekpi tbody").append('<tr>');
      for (var i=0;i<kpi_object_list.length;i++) {
         var row = tablekpi.insertRow(1);
         row.name = i;
         var cell1 = row.insertCell(0);
         var cell2 = row.insertCell(1);
         var cell3 = row.insertCell(2);
         var cell4 = row.insertCell(3);
         var cell5 = row.insertCell(4);
         var cell6 = row.insertCell(5);
         var cell7 = row.insertCell(6);

         var div1 = document.createElement("div");
         div1.id = 'divkpi'+i;
         $(div1).addClass("form-group has-error divkpi");

         var cell8 = document.createElement("input");
         cell8.type='number';
         cell8.name='inputkpi';
         $(cell8).addClass('form-control');
         cell8.placeholder='Enter KPI value';

         $(cell8).change(function() {
            validateKPI ($(this).val() , $(this).parent().closest('div'));
         });

         div1.appendChild(cell8);

         var name_kpi;
         var remark;

         switch (kpi_object_list[i].name) {
            case "video_accessibility" :
               name_kpi = "Video Accessibility";
               break;
            case "video_freeze_rate":
               name_kpi = "Video Freeze Rate";
               break;
         }

         switch (cell7.innerHTML = kpi_object_list[i].remarks) {
            case "":
               remark = "-";
               break;
         }

         cell1.innerHTML = kpi_object_list[i].name;// name_kpi;
         cell2.innerHTML = kpi_object_list[i].service;
         cell3.innerHTML = kpi_object_list[i].warning_threshold;
         cell4.innerHTML = kpi_object_list[i].expected_best_value;
         cell5.innerHTML = kpi_object_list[i].critical_threshold;
         cell6.innerHTML = kpi_object_list[i].expected_worst_value;
         cell7.innerHTML = kpi_object_list[i].remarks;//remark;
         row.appendChild(div1);
         //console.log("rows :",i);
      }
      $("#tablekpi tbody tr").last().remove();
   });
}

function insert_groups_Into_Form() {
   $.get("./Group_List_Servlet", function(data){
      console.log ("Inserting Groups");
      group_list=data;
      group_object_list = JSON.parse(data);
      var field = document.forms.predictionForm.elements;
      var tablegroup = document.getElementById("tablegroup");

      for (var i=0;i<group_object_list.length;i++) {
         var row = tablegroup.insertRow(1);
         var cell1 = row.insertCell(0);
         var cell2 = row.insertCell(1);

         var div1 = document.createElement("div");
         div1.id = 'divkpi'+i;
         $(div1).addClass("form-group has-error col-lg-4 divgroup");

         var cell3 = document.createElement("input");
         cell3.type="number";
         $(cell3).addClass("form-control");
         cell3.id="value"+i;
         cell3.placeholder='Enter Group Share';
         cell3.value=0;

         $(cell3).change(function() {
            var total = 0;
            $("#tablegroup .divgroup").each(function () {
               total = total + Number ($(this).find("input").val());
            });

            $("#tablegroup .divgroup").each(function () {
               $($(this).find("input")).each(function() {
                  validateGroupShare (total , $(this).parent().closest('div'));
               }
            )});
         });

         div1.appendChild(cell3);
         var name_group;
         var remark;

         switch (group_object_list[i].name){
         case "silver_domestic":
            name_group = "Silver Domestic";
            break;
         case "bronze_domestic":
            name_group = "Bronze Domestic";
            break;
         case "bronze_young":
            name_group = "Bronze Young";
            break;
         case "bronze_corporate":
            name_group = "Bronze Corporate";
            break;
         case "silver_corporate":
            name_group = "Silver Corporate";
            break;
         case "Gold":
            name_group = "Gold";
            break;
         }

         switch (group_object_list[i].remarks){
         case "":
            remark = "-";
            break;
         }

         cell1.innerHTML = group_object_list[i].name;//name_group;
         cell2.innerHTML = group_object_list[i].remarks//"-";
         row.appendChild(div1);
      }

      $("#tablegroup tbody tr").last().remove();
   });
}

function generateJson() {
   console.log ("generateJson");
   var validity=false;
   validity = check_input_data();

   if (validity == false) {
      alert("Validation error");
   } else {
      var prediction = {};
      var ReportId = document.forms["predictionForm"]["reportId"].value;//
      //var TimeStampDate = new Date(document.forms["predictionForm"]["timestamp"].value);
      //var TimeStamp = TimeStampDate.getTime();
      var TimeStamp = new Date().getTime()
      var Location = document.forms["predictionForm"]["sel1"].value;//
      var Location_str = Location.toString();
      var StartDate = document.forms["predictionForm"]["starttimecalendar"].value;//
      var EndDate = document.forms["predictionForm"]["endtimecalendar"].value;//
      var StartTime = document.forms["predictionForm"]["starttime"].value;//
      var EndTime = document.forms["predictionForm"]["endtime"].value;//
      var Confidence = parseFloat(document.forms["predictionForm"]["confidence"].value)/100;//

      prediction.reportID   = ReportId;
      //prediction.service    = "video";
      prediction.confidence = Confidence ;
      prediction.timestamp  = TimeStamp;
      
      var Location_array    = Location_str.split(':');
      var Location_cellId   = Location_array[1].replace(" ", ""); //Location_CellId has the value for the selected cell
      var Location_array    = new Array()
      Location_array.push (extracting_location_parameters(Location_cellId));
      prediction.location   = Location_array;

      // Service object management
      var service = {};
      service["name"] = null;
      service["kpi"] = [];
      var service_kpi=[];

      // we analyze the form and extract KPI values. kpi is an object with three keys: service, name and value.
      // The value of each of them is a list of service names, kpi names and kpi values (in the same order)
      var kpi = get_service_kpi ();

      // TODO: if no change in the report_deliver JSON schema is carried out, only KPI's from a same type of services
      // are supported. Therefore, the remaining KPI's are discarded. Here we set valid_kpi to 'video'
      var valid_kpi = 'video';
      for (i=0;i<kpi.name.length;i++){
    	  var service_name = kpi["service"][i].toLowerCase()
    	  if ((kpi["value"][i].length > 0) & (service_name === valid_kpi)) {
              service_kpi.push ({"name":kpi["name"][i],"value":parseInt(kpi["value"][i])});
         }
      }

      service["name"] = valid_kpi;
      service["kpi"] = service_kpi;

      // Group KPIs by service name
      prediction.service = service;
      
      // Validity handling
      var validity = {};
      validity_start = new Date(parseInt(StartDate.slice(6, 10)), parseInt(StartDate.slice(3, 5))-1, parseInt(StartDate.slice(0, 2)), parseInt(StartTime.slice(0,2)), parseInt(StartTime.slice(3,5)))
      //validity.start = validity_start.toString().split('(')[0].slice(0, -1);
      var ValidityStartDate = new Date(validity_start.toString().split('(')[0].slice(0, -1));
      validity.start = ValidityStartDate.getTime();
      
      validity_end = new Date(parseInt(EndDate.slice(6, 10)), parseInt(EndDate.slice(3, 5))-1, parseInt(EndDate.slice(0, 2)), parseInt(EndTime.slice(0,2)), parseInt(EndTime.slice(3,5)))
      //validity.end = validity_end.toString().split('(')[0].slice(0, -1);
      var ValidityEndDate = new Date(validity_end.toString().split('(')[0].slice(0, -1));
      validity.end = ValidityEndDate.getTime();
      prediction.validity = validity;

      // Group handling
      var groups = {};
      var groups_all = [];
      groups = get_groups_share();

      for (i=0;i<groups.name.length;i++){
         groups_all.push ({"name":groups["name"][i],"share":parseInt(groups["share"][i])});
      }

      prediction.groups = groups_all;

      predictionJSON=JSON.stringify(prediction);
      console.log('JSON to be sent:');
      console.log(predictionJSON);

      //Send prediction to the server
      var output = $.ajax ({
                            type: 'POST',
                            url: "/PGF_Server/rest/sessions",
                            data: predictionJSON,
                            contentType: 'application/json',
      });
      output.done (function (data) {
         $('#container_main').css ('display', 'none');
         //$('#json').css ('display', 'none');
         $('#success_container').css ('display', 'block');
         $('#json').text(predictionJSON);
      });
      output.fail (function (xhr, ajaxOptions, thrownError) {
         alert("Error: " + xhr.status + "\n" +
               "Message: " + xhr.statusText + "\n" +
               "Response: " + xhr.responseText + "\n" + thrownError);
      });
   }
}

function validateKPI (inputField, divvalue) {
   if ((inputField<=100) && (inputField>=0)) {
      $(divvalue).removeClass("has-error");
      $(divvalue).addClass ("has-success");
      kpis=true;
   } else {
      $(divvalue).removeClass("has-success");
      $(divvalue).addClass ("has-error");
      kpis=false;
   }
   return kpis;
}

function validateGroupShare (total, divvalue) {
   $("#share").text("Share : " + total + "%");

   if (total == 100) {
     $(divvalue).removeClass("has-error");
     $(divvalue).addClass ("has-success");
     $("#share").css("color", "green");
     groups=true;
   } else {
     $(divvalue).removeClass("has-success");
     $(divvalue).addClass ("has-error");
     $("#share").css("color", "red");
     groups=false;
   }
   return groups;
}

function validateConfidence(inputField) {
   if ((inputField<=100) && (inputField>=0)) {
      $("#confidence-div").removeClass("has-error");
      $("#confidence-div").addClass ("has-success");
      confidence=true;
   } else {
      $("#confidence-div").removeClass("has-success");
      $("#confidence-div").addClass ("has-error");
      confidence=false;
   }
   return confidence;
}

function validateStartTime(inputField) {
   var regularExpression = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])?$/.test(inputField);

   if (regularExpression) {
      $("#st").removeClass("has-error");
      $("#st").addClass ("has-success");
      start_time=true;
   } else {
      $("#st").removeClass("has-success");
      $("#st").addClass ("has-error");
      start_time=false;
   }
   return regularExpression;
}

function validateEndTime(inputField) {
   var regularExpression = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])?$/.test(inputField);

   if (regularExpression) {
      $("#et").removeClass("has-error");
      $("#et").addClass ("has-success");
      end_time=true;
   } else {
      $("#et").removeClass("has-success");
      $("#et").addClass ("has-error");
      end_time=false;
   }
   return regularExpression;
}

function validateStartDate(sdate) {
   var startDate_Valid = false;
   var   StartDate = sdate.setHours(0,0,0,0);
   var now = new Date().setHours(0,0,0,0);

   if ((StartDate == now) || (StartDate > now))  {
      $("#sd").removeClass("has-error");
      $("#sd").addClass ("has-success");
      start_date=true;
      var day = ("0" + sdate.getDate()).slice(-2);
      var month = ("0" + (sdate.getMonth() + 1)).slice(-2)
      var year = sdate.getFullYear();
      var enddate = day + '-' +  month + '-' + year;
      $("#endtimecalendar").val(enddate);
      validateEndDate(sdate,sdate);
   } else {
      $("#sd").removeClass("has-success");
      $("#sd").addClass ("has-error");
      start_date=false;
   }
   return start_date;
}

function validateEndDate(edate, sdate) {
   var endDate_Valid = false;
   var StartDate = sdate.setHours(0,0,0,0);
   var EndDate = edate.setHours(0,0,0,0);
   var now = new Date().setHours(0,0,0,0);

   if ((EndDate >= StartDate) && (EndDate >= now) )  {
      $("#ed").removeClass("has-error");
      $("#ed").addClass ("has-success");
      end_date=true;
   } else {
      $("#ed").removeClass("has-success");
      $("#ed").addClass ("has-error");
      end_date=false;
   }
   return end_date;
}

function check_input_data() {
   if(confidence && start_time && end_time && start_date && end_date && kpis && groups)
      return true;
   else
      return false;
  }

function get_groups_share () {
   var groups = {};
   var group_name = [];
   var group_share = [];

   console.log ("Groups and share Selection " );

   //for each tr located within the body of the table kpi get the kpi name and the related service and introduce them into the related array
   $("#tablegroup tbody tr").each(function () {
      group_name.push ($(this).find("td:first").html());
   });

   $("#tablegroup .divgroup").each(function () {
   group_share.push($(this).find("input").val());
   });

   groups["name"]=group_name;
   groups["share"]=group_share;
   return groups;
}

function get_service_kpi () {
   var kpi = {};
   kpi_index=0;
   kpi_service = [];
   kpi_name = [];
   kpi_value = [];
   console.log ("KPI Selection " );

   //for each tr located within the body of the table kpi get the kpi name and the related (type of) service 
   //and introduce them into the related array
   $("#tablekpi tbody tr").each(function () {
      kpi_name.push ($(this).find("td:first").html());
      kpi_service.push($(this).find("td:eq(1)").html());
   });

   //getting values from the input field
   $("#tablekpi .divkpi").each(function () {
      kpi_value.push($(this).find("input").val());
   });

   //grouping data coming from the kpi table into the kpi object
   kpi["service"] = kpi_service;
   kpi["name"] =  kpi_name;
   kpi["value"] = kpi_value;

   return kpi;
}

function extracting_location_parameters(Location_cellId) {
   var Register = null;

   $.ajax({
      url: "/AnalyticsNodeSimulator/Location_List_From_DB",
      type: 'get',
      dataType: 'html',
      async: false,
      success: function(data) {
         var Location_List=JSON.parse(data);
         for (i=0;i<Location_List.length;i++) {
            var Location_Id = Location_List[i].cellId.replace(" ", "");
            if (Location_Id == Location_cellId) {
               Register = Location_List[i];
               console.log ("Location Register1: " +  Register.id);
               break;
            }
         }
      }
   });
   console.log ("Location Register3: " +  Register.city);
   return (Register.id);
}

function sendJSON () {
   var request = new XMLHTTPRequest();
   var   SessionId = document.forms["predictionForm"]["reportId"].value;
   document.write(SessionId);
}

window.onload = function() {
	if ($(location).attr('href').includes(MAIN_PAGE)) {
		insert_ReportId_Into_Form();
		insert_LocationList_Into_Form();
		insert_TimeStamp_Into_Form();
		insert_services_and_kpis_Into_Form();
		insert_groups_Into_Form();

		$("#starttimecalendar").change(function() {
			validateStartDate ($("#starttimecalendar").datepicker("getDate"));
		});

		$("#endtimecalendar").change(function() {
			validateEndDate ($("#endtimecalendar").datepicker("getDate"), $("#starttimecalendar").datepicker("getDate") )
		});

		$("#starttime").change(function() {
			validateStartTime ($("#starttime").val());
		});

		$("#endtime").change(function() {
			validateEndTime ($("#endtime").val());
		});

		$("#confidence").change(function() {
			validateConfidence ($("#confidence").val());
		});
	}
};
