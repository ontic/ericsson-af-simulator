<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<style type="text/css">
#logo_ontic {
    background-image: url("./img/ontic.png");
    background-repeat: no-repeat;
    background-size: contain;
    width: 120px;
    height: 60px;
    margin: 0;
    padding: 0;
}
#logo_ericsson {
    background-image: url("./img/Ericsson_logo.png");
    background-repeat: no-repeat;
    background-size: contain;
    width: 120px;
    height: 100px;
    margin: 0;
    padding: 0;
}
</style>
<script type="text/javascript" src="./scripts.js"></script>
<title>Analytics Node</title>
</head>

<body>
  <div class="container">
      <!-- The justified navigation menu is meant for single line per list item.
           Multiple lines will require custom code not provided by Bootstrap. -->
      <div class="masthead">
        <h3 class="text-muted">Adaptive Quality of Experience (AQoE)</h3>
        <nav>
          <ul class="nav nav-justified">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#">Projects</a></li>
            <li><a href="#">Services</a></li>
            <li><a href="#">Downloads</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
        </nav>
      </div>

      <!-- Jumbotron -->
      <div class="jumbotron">
          	<div class="row">
     	 <div class="col-md-3" id="logo"></div><div class="col-md-9"></div>
    	</div>
        <h1>Analytics Function (AF) Simulator</h1>
        <p class="lead">Creation of simulated QoE simulation reports and submission to the PGF</p>
        <p><a class="btn btn-lg btn-success" href="./GeneratePrediction.jsp" role="button">Get started</a></p>
      </div>
      
      <!-- Site footer -->
      <footer class="footer"></footer>

    </div> <!-- /container -->

</body>

<script>
$(function() {
    $('#logo').attr('id','logo_'+LOGO);
});
</script>
</html>