The Analytics Function Simulator is a testing tool that, through a web-based 
interface, enables the creation of QoE degradation reports as if they were
created from a regular AF and their delivery to the PGF over the AF-PGF 
RESTful interface (IF3-2).

