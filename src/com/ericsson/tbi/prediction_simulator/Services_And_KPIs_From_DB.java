/**
 * 
 */
package com.ericsson.tbi.prediction_simulator;

import java.util.ArrayList;
import java.util.List;

import com.ericsson.tbi.json.Analytics.database.DBManager;
import com.ericsson.tbi.json.Analytics.database.DBManagerFactory;
import com.google.gson.Gson;
import com.ericsson.tbi.json.newregister.KPI_Record;

/**
 * @author ECEALBM
 *
 */
public class Services_And_KPIs_From_DB {

	
	private Gson gson = new Gson();

	/**
	 * 
	 * @author ECEALBM
	 * @throws Exception 
	 */
	
	public String Services_And_KPIs_From_DB() throws Exception {
		// TODO Auto-generated method stub
		
			DBManager db = DBManagerFactory.getDBManager();
			List<KPI_Record> kpi_record_list = new ArrayList<KPI_Record>();
			
			
		try {
		
		db.connect();
		kpi_record_list = db.get_services_and_kpis_from_db();
		
		
		System.out.println (kpi_record_list.toString());
		
		   
		} catch (Exception e)
		{
		System.out.println ("Exception database access " + e);
		}finally
		{
			db.disconnect();
		}
		
		
		return gson.toJson(kpi_record_list);
	    
	}
	
	
	
	
}
