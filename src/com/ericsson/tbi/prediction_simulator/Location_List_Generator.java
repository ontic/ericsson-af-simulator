/**
 * 
 */
package com.ericsson.tbi.prediction_simulator;

import java.util.ArrayList;
import java.util.List;

import com.ericsson.tbi.json.Analytics.database.DBManager;
import com.ericsson.tbi.json.Analytics.database.DBManagerFactory;
import com.google.gson.Gson;
import com.ericsson.tbi.json.newregister.Location;

/**
 * @author ECEALBM
 *
 */
public class Location_List_Generator {

	
	private Gson gson = new Gson();

	/**
	 * 
	 * @author ECEALBM
	 * @throws Exception 
	 */
	public String Location_List_From_DB() throws Exception {
		// TODO Auto-generated method stub
		
			DBManager db = DBManagerFactory.getDBManager();
			List<Location> location_list = new ArrayList<Location>();
			
			
		try {
		
		db.connect();
		location_list = db.get_locations_from_db();
		
		
		System.out.println (location_list.toString());
		
		   
		} catch (Exception e)
		{
		System.out.println ("Exception converter " + e);
		} finally{
			
			db.disconnect();
		}
	
		
		
		return gson.toJson(location_list);
	    
	}
	
	
	
	
}
