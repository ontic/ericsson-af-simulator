/**
 * 
 */
package com.ericsson.tbi.prediction_simulator;

import java.util.ArrayList;
import java.util.List;

import com.ericsson.tbi.json.Analytics.database.DBManager;
import com.ericsson.tbi.json.Analytics.database.DBManagerFactory;
import com.ericsson.tbi.json.newregister.Groups_Record;
import com.google.gson.Gson;

/**
 * @author ECEALBM
 *
 */
public class Group_List_Generator {

	private Gson gson = new Gson();
	
	/**
	 * 
	 * @author ECEALBM
	 * @throws Exception 
	 */
	public String Group_List_From_DB() throws Exception {
		// TODO Auto-generated method stub
		
			DBManager db = DBManagerFactory.getDBManager();
			List<Groups_Record> group_list = new ArrayList<Groups_Record>();
			
			
		try {
		
		db.connect();
		group_list = db.get_groups_from_db();
		
		
		System.out.println (group_list.toString());
		
		   
		} catch (Exception e)
		{
		System.out.println ("Exception converter " + e);
		} finally
		{
			db.disconnect();
		}
		
		
		
		return gson.toJson(group_list);
	    
	}
	
	

	
	
}
