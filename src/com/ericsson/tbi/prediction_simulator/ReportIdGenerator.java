package com.ericsson.tbi.prediction_simulator;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ReportIdGenerator
 */
@WebServlet("/ReportIdGenerator")
public class ReportIdGenerator extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReportIdGenerator() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	Report_Id_Generator generate_Report_Id = new Report_Id_Generator();
		
//		HttpSession session = request.getSession(true);
//		session.setMaxInactiveInterval(5);
//		
//				
		String reportId = generate_Report_Id.get_report_id ();
		
		
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		out.println (reportId);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
