/*
+++ 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
+++                                                                         +++ 
+++ COPYRIGHT (c) Ericsson AB 2008                                          +++ 
+++                                                                         +++ 
+++ The copyright to the computer Program(s) herein is the                  +++ 
+++ property of Ericsson AB, Sweden. The program(s) may be                  +++ 
+++ used and or copied only with the written permission of                  +++ 
+++ Ericsson AB, or in accordance with the terms and conditions             +++ 
+++ stipulated in the agreement contract under which the                    +++ 
+++ program(s) have been supplied.                                          +++ 
+++                                                                         +++ 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
+++                                                                         +++
*/

package com.ericsson.tbi.prediction_simulator;

import java.util.*;

//https://blogs.oracle.com/enterprisetechtips/entry/consuming_restful_web_services_with

public class Report_Id_Generator {

	private static boolean LOCAL = true;
	
	/**
	 * This is a simple method that actuates as a simulator of an analytics function, reading a JSON file from a file and adapting it to update some fields such are timestamp and reportId
	 * later on the method sends to the POST resource on the PGF server the JSON that will be stored on the database and create a new sessionId.
	 * @param args
	 * @throws IOException
	 * @author ECEALBM
	 */
	
	/**
	 * 
	 * @return random Report_id
	 * @author ECEALBM
	 */
	
    public String get_report_id (){
    	
  	String report_id= UUID.randomUUID().toString();
   	System.out.println ("Report id1 " + report_id);
   	return report_id;
     }
    
    
    public String get_Location_List_From_DB (){
    	
      	String report_id= UUID.randomUUID().toString();
       	System.out.println ("Report id1 " + report_id);
       	return report_id;
         }
}