/**
 * 
 */
package com.ericsson.tbi.json.newregister;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ECEALBM
 *
 */
public class New_Register {
	
	public String reportId;
	public String timestamp;
	public float confidence;
	public Service service = new Service();
	public List <Validity> validity = new ArrayList <Validity>();
	public List <Groups> groups = new ArrayList <Groups>();

}
