/**
 * 
 */
package com.ericsson.tbi.json.newregister;

/**
 * @author ECEALBM
 *
 */
public class Groups_Record {

	private String name;
	private String remarks;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
