/**
 * 
 */
package com.ericsson.tbi.json.newregister;

/**
 * @author ECEALBM
 *
 */
public class KPI_Record {

	private String name = null;
 	private String service = null;
 	private Double warning_threshold = 0.0;
 	private Double expected_best_value=0.0;
 	private String remarks = null;
 	private Double critical_threshold = 0.0;
 	private Double expected_worst_value =0.0;
	
 	
 	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public Double getWarning_threshold() {
		return warning_threshold;
	}
	public void setWarning_threshold(Double warning_threshold) {
		this.warning_threshold = warning_threshold;
	}
	public Double getExpected_best_value() {
		return expected_best_value;
	}
	public void setExpected_best_value(Double expected_best_value) {
		this.expected_best_value = expected_best_value;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Double getCritical_threshold() {
		return critical_threshold;
	}
	public void setCritical_threshold(Double critical_threshold) {
		this.critical_threshold = critical_threshold;
	}
	public Double getExpected_worst_value() {
		return expected_worst_value;
	}
	public void setExpected_worst_value(Double expected_worst_value) {
		this.expected_worst_value = expected_worst_value;
	}
 	
	
}
