package com.ericsson.tbi.json.Analytics.database;

import java.util.List;

import com.ericsson.tbi.json.newregister.Groups_Record;
import com.ericsson.tbi.json.newregister.KPI_Record;
import com.ericsson.tbi.json.newregister.Location;

public abstract class DBManager {

	abstract public void connect() throws Exception;
      
	abstract public void disconnect() throws Exception;
	  
	abstract public List <Location> get_locations_from_db() throws Exception;;

	abstract public List <KPI_Record> get_services_and_kpis_from_db () throws Exception;

	abstract public List<Groups_Record> get_groups_from_db() throws Exception;
}