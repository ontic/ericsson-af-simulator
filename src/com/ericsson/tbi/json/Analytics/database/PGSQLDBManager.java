/**
 * @author ECEALBM
 *
 */
package com.ericsson.tbi.json.Analytics.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.postgresql.util.PSQLException;

import com.ericsson.tbi.json.newregister.Groups_Record;
import com.ericsson.tbi.json.newregister.KPI_Record;
import com.ericsson.tbi.json.newregister.Location;

public class PGSQLDBManager extends DBManager {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.tbi.json.Analytics.database.DBManager#connect()
	 */
	private Connection conn = null;

	@Override
	public void connect() throws Exception {
		// TODO Auto-generated method stub

		String url = "jdbc:postgresql://127.0.0.1:5432/pgf";
		Class.forName("org.postgresql.Driver");
		Properties props = new Properties();
		props.setProperty("user", "pgf");
		props.setProperty("password", "pgf");
		System.out.println("Starting connection");
		conn = DriverManager.getConnection(url, props);
		System.out.println("Connection got");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.tbi.json.Analytics.database.DBManager#disconnect()
	 */
	@Override
	public void disconnect() throws Exception {
		// TODO Auto-generated method stub

		conn.close();

	}

	/**
	 * This method extract locations from old json files and stores them into
	 * the db to be used as reference in the PGF PoC
	 * 
	 * @see com.ericsson.tbi.json.Analytics.database.DBManager#store_locations()
	 */
	@Override
	public List<Location> get_locations_from_db() throws Exception {
		// TODO Auto-generated method stub

		PreparedStatement preparedStmnt = null;
		ResultSet rs = null;
		List<Location> location_list = new ArrayList<Location>();

		try {
			// Degradation report is the table that stores the 1..n data related
			// to the kpis
			preparedStmnt = conn
					.prepareStatement("SELECT * FROM technologies.location");

			preparedStmnt.execute();
			rs = preparedStmnt.getResultSet();

			while (rs.next()) {

				Location location_register = new Location();

				location_register.setId(rs.getString("id"));
				location_register.setTechnology(rs.getString("technology"));
				location_register.setLat(rs.getString("lat"));
				location_register.setLon(rs.getString("lon"));
				location_register.setCity(rs.getString("city"));
				location_register.setRemarks(rs.getString("remarks"));
				location_register.setCellId(rs.getString("cell_id"));

				location_list.add(location_register);
			}
		} catch (Exception e) {

			System.out.println("Error" + e);
		}

		return location_list;

	}

	@Override
	public List<KPI_Record> get_services_and_kpis_from_db() throws Exception {

		PreparedStatement preparedStmnt = null;
		ResultSet rs = null;
		List<KPI_Record> kpi_record_list = new ArrayList<KPI_Record>();

		System.out.println("get_services_and_kpis_from_db ()");

		try {

			preparedStmnt = conn.prepareStatement("SELECT * "
					+ "FROM technologies.kpi");

			preparedStmnt.execute();
			rs = preparedStmnt.getResultSet();

			while (rs.next()) {

				KPI_Record kpi_record = new KPI_Record();

				kpi_record.setName(rs.getString("name"));
				kpi_record.setService(rs.getString("service"));
				kpi_record.setWarning_threshold(rs
						.getDouble("warning_threshold"));
				kpi_record.setExpected_best_value(rs
						.getDouble("expected_best_value"));
				kpi_record.setRemarks(rs.getString("remarks"));
				kpi_record.setCritical_threshold(rs
						.getDouble("critical_threshold"));
				kpi_record.setExpected_worst_value(rs
						.getDouble("expected_worst_value"));
				kpi_record_list.add(kpi_record);

			}

		} catch (PSQLException e) {
			System.out.println("Error" + e);
		} finally {
			if (preparedStmnt != null && !preparedStmnt.isClosed())
				preparedStmnt.close();
		}
		return kpi_record_list;
	}

	@Override
	public List<Groups_Record> get_groups_from_db() throws Exception {

		PreparedStatement preparedStmnt = null;
		ResultSet rs = null;
		List<Groups_Record> groups_record_list = new ArrayList<Groups_Record>();

		System.out.println("get_groups_from_db ()");

		try {

			preparedStmnt = conn.prepareStatement("SELECT * "
					+ "FROM subscriber.customer_segment");

			preparedStmnt.execute();
			rs = preparedStmnt.getResultSet();

			while (rs.next()) {

				Groups_Record groups_record = new Groups_Record();
				groups_record.setName(rs.getString("name"));
				groups_record.setRemarks(rs.getString("remarks"));

				groups_record_list.add(groups_record);
			}

		} catch (PSQLException e) {
			System.out.println("Error" + e);
		} finally {
			if (preparedStmnt != null && !preparedStmnt.isClosed())
				preparedStmnt.close();
		}
		return groups_record_list;

	}

}
